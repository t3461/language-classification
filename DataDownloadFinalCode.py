import youtube_dl
from pytube import Playlist
from moviepy.editor import *
import os
from pydub import AudioSegment


#Tali's folders
DOWNLOAD_DIR_ENGLISH_WAV = 'audio\\EnglishWAV\\'
DOWNLOAD_DIR_ENGLISH_MP3 = 'audio\\EnglishMP3\\'
DOWNLOAD_DIR_FRENCH_MP3 = 'audio\\FrenchMP3\\'
DOWNLOAD_DIR_FRENCH_WAV = 'audio\\FrenchWAV\\'
AUDIO_FILE_ENDING = ".mp3"

EnglishPlaylist = Playlist('https://youtube.com/playlist?list=PLnM2ToE_3IomNTOK8AUA_ZX3Dx86wYYva')
FrenchPlaylist = Playlist('https://www.youtube.com/playlist?list=PLnM2ToE_3IomoJse2IXVOeQtNTp_Uemnq')

def downloading(playListPath, language, downloadDir):

    """
    this function download a clips from youtube as a audio.
    Then cut the audio file to 10 sec and save the short audio file
    :param playListPath(playlist): playlist to downlooad
    :param language(str): the language of the video in the playlist
    :param downloadDir(str): dir to put all the audio
    :return: none
    """

    cwd = os.getcwd()
    video_number = 1
    for url in playListPath.video_urls:
        try:
            video_info = youtube_dl.YoutubeDL().extract_info( url=url, download=False)
            filename = f"{video_info['title'][2:4]}.mp3"
            options = {
                'format': 'bestaudio/best',
                'keepvideo': False,
                'outtmpl': filename,
            }

            with youtube_dl.YoutubeDL(options) as ydl:
                ydl.download([video_info['webpage_url']])

            print("Download complete... {}".format(filename))
            new_name = f'{language}_video{video_number}{AUDIO_FILE_ENDING}'
            audioclip = AudioFileClip(filename).subclip(10, 20)
            audioclip.write_audiofile(f'{downloadDir}{new_name}')
            video_number += 1
            audioclip.close()

            deleting([filename])

        except Exception as ex:
             template = "An exception of type {0} occurred. Arguments:\n{1!r}"
             message = template.format(type(ex).__name__, ex.args)
             print (message)


def deleting(flist):
    """
    this function delete musics video
    :param flist (list): list of files that need to be deleted
    """

    for video in flist:
        os.remove(video)


def mp3ToWavFile(language, dirPath, downloadDir):
    """
    the function get path to a dir with a audio files and convert them to wav file
    :param language (str): the language of the audio files
    :param dirPath (str): path to the dir with the mp3 audio files
    :param downloadDir: path to the dir that will contain all the wav files.
    :return: none
    """
    video_number = 1
    try:
        for filename in os.listdir(dirPath):
            print(video_number)
            sound = AudioSegment.from_mp3(f'{dirPath}{filename}')
            sound.export(f'{downloadDir}{language}_video{video_number}.wav', format="wav")
            video_number+=1

    except Exception as ex:
        print("Error")

if __name__ == '__main__':

    #English
    #downloading(playListPath = EnglishPlaylist, language = "English", downloadDir = DOWNLOAD_DIR_ENGLISH)
    #French
    #downloading(playListPath = FrenchPlaylist, language = "French", downloadDir = DOWNLOAD_DIR_FRENCH)
    #mp3ToWavFile(language = "English", dirPath = DOWNLOAD_DIR_ENGLISH_MP3, downloadDir = DOWNLOAD_DIR_ENGLISH_WAV)
    mp3ToWavFile(language="French", dirPath=DOWNLOAD_DIR_FRENCH_MP3, downloadDir=DOWNLOAD_DIR_FRENCH_WAV)

