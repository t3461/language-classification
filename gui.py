import numpy as np
import extract_data as ed
import tensorflow.keras as keras
from tkinter import *
import tkinter.font as font
from tkinter import filedialog
from pydub import AudioSegment

#global variables
model = keras.models.load_model('model.h5')
chosenFilesPath = ""
predictReasult = 0
lan = ""
labels = {1 : 'English', 2 : 'French'}

def predictLanguage():
    """
    the function will predict the language of the file that
    the user enter
    :return: none
    """
    global chosenFilesPath
    global labels
    global lan

    #change the file to a wav file
    chosenFilesPath = ed.convert_any_file_to_wav(chosenFilesPath)
    chosenFilesPath = AudioSegment.from_wav(chosenFilesPath)
    chosenFilesPath = chosenFilesPath[0:10000]
    chosenFilesPath.export('newSong.wav', format="wav")

    #preparing the audio file for the lerning machine
    audio_segment, sample_rate = ed.load_audio_file('newSong.wav')
    spec = ed.to_integer(ed.spectrogram(audio_segment)).tolist()
    spec = np.array(spec)
    spec = spec[np.newaxis, ...]

    #predict the langauge
    prediction = model.predict(spec)
    predicted_index = np.argmax(prediction, axis=1)
    print("Predicted label: {}".format(predicted_index))
    print("File is : {}".format(labels[predicted_index[0]]))
    lan = labels[predicted_index[0]]

def browseFiles():
    global chosenFilesPath
    global lan
    chosenFilesPath = filedialog.askopenfilename(initialdir="/", title="Select a File",
                                                 filetypes=(("Audio files","*.mp3*"), ("Audio files","*.wav*"), ("all files","*.*")))
    predictLanguage()
    label_file_explorer.configure(text=lan)

#gui
window = Tk()
window.title('File Explorer')
window.geometry("500x250")
window.config(background="pink")

textFont = font.Font(family='Cambria', size=20, weight='bold')
buttonsFont = font.Font(family='Courier', size=8, weight='bold')
label_file_explorer = Label(window, text="Choose Your Audio File :)", width=30, height=4, bg="pink", font=textFont, fg="purple")
button_explore = Button(window, text="Browse Files", font=buttonsFont, command=browseFiles)
button_exit = Button(window, text="Exit", font=buttonsFont, command=exit)

label_file_explorer.grid(column=1, row=1)
button_explore.grid(column=1, row=2)
button_exit.grid(column=1, row=5)

# Let the window wait for any events
window.mainloop()
